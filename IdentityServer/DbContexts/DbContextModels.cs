﻿using System.Data.Entity;
using IdentityServer.Models;

namespace IdentityServer.DbContexts
{
    public class IsContext : DbContext
    {
        public IsContext()
            : base("IdentityServerConnection")
        {
            Database.SetInitializer<IsContext>(null);
        }

        public DbSet<ClientProfile> ClientProfiles { get; set; }
        public DbSet<ScopeProfile> ScopeProfiles { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }

    }
}