﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using IdentityServer3.Core.Models;

namespace IdentityServer.Models
{

    [Table("is_Clients")]
    public class ClientProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? AbsoluteRefreshTokenLifetime { get; set; } //in seconds ( Defaults to 2592000 seconds / 30 days)
        public int? AccessTokenLifetime { get; set; } //in seconds (Defaults to 3600 seconds / 1 hour)
        public string AccessTokenType { get; set; } //"Jwt" or "Reference"
        public bool AllowAccessToAllCustomGrantTypes { get; set; } //allow access to all grant types
        public bool AllowAccessToAllScopes { get; set; } //allow access to all scopes
        public bool AllowClientCredentialsOnly { get; set; } //allow client credentials only 
        public string AllowedCorsOrigins { get; set; } //JSON String
        public string AllowedCustomGrantTypes { get; set; } //JSON String
        public string AllowedScopes { get; set; } //JSON String
        public bool AllowRememberConsent { get; set; } //allow remember consent on consent screen
        public bool AlwaysSendClientClaims { get; set; } //always send client claims with access token
        public int? AuthorizationCodeLifetime { get; set; } // in seconds (defaults to 300 seconds / 5 minutes)
        public string Claims { get; set; } //JSON String
        public string ClientId { get; set; } //unique client id
        public string ClientName { get; set; } //display name
        public string ClientSecrets { get; set; } //JSON String
        public string ClientUri { get; set; } //client url
        public bool Enabled { get; set; } //Enables client to be used
        public bool EnableLocalLogin { get; set; } //allow local login
        public string Flow { get; set; } //"AuthorizationCode", "Implicit", "Hybrid", "ResourceOwner", "ClientCredentials", or "Custom"
        public string IdentityProviderRestrictions { get; set; } //JSON String
        public int? IdentityTokenLifetime { get; set; } //in seconds (Default to 300 seconds / 5 minutes)
        public bool IncludeJwtId { get; set; } //include Jwt id in Jwt access token
        public string LogoUri { get; set; } //client logo url
        public bool LogoutSessionRequired { get; set; } //Requires user's session id to be sent to logout url
        public string LogoutUri { get; set; } //client logout url
        public string PostLogoutRedirectUris { get; set; } //JSON String
        public bool PrefixClientClaims { get; set; } //add prefix to client claims
        public string RedirectUris { get; set; } //JSON String
        public string RefreshTokenExpiration { get; set; } //"Absolute" or "Sliding"
        public string RefreshTokenUsage { get; set; } //"OneTimeOnly" or "ReUse"
        public bool RequireConsent { get; set; } //Displays consent screen
        public int? SlidingRefreshTokenLifetime { get; set; } //in seconds (Defaults to 1296000 seconds / 15 days)
        public bool UpdateAccessTokenClaimsOnRefresh { get; set; } //Update token on refresh
    }

    public class ClientModel
    {
        public bool IsEnabled { get; set; } //Enables client to be used        
        public string ClientId { get; set; } //unique client id
        public string ClientName { get; set; } //display name        
        public string ClientUri { get; set; } //client url                
        public string Flow { get; set; } //"AuthorizationCode", "Implicit", "Hybrid", "ResourceOwner", "ClientCredentials", or "Custom"                   
        public string LogoUri { get; set; } //client logo url        
        public string LogoutUri { get; set; } //client logout url                     
        public string RefreshTokenExpiration { get; set; } //"Absolute" or "Sliding"
        public string RefreshTokenUsage { get; set; } //"OneTimeOnly" or "ReUse"
        public string AccessTokenType { get; set; } //"Jwt" or "Reference"
        public int? SlidingRefreshTokenLifetime { get; set; } //in seconds (Defaults to 1296000 seconds / 15 days)        
        public int? AbsoluteRefreshTokenLifetime { get; set; } //in seconds ( Defaults to 2592000 seconds / 30 days)
        public int? AccessTokenLifetime { get; set; } //in seconds (Defaults to 3600 seconds / 1 hour)       
        public int? IdentityTokenLifetime { get; set; } //in seconds (Default to 300 seconds / 5 minutes)
        public int? AuthorizationCodeLifetime { get; set; } // in seconds (defaults to 300 seconds / 5 minutes)       
        public bool RequireConsent { get; set; } //Displays consent screen
        public bool LogoutSessionRequired { get; set; } //Requires user's session id to be sent to logout url
        public bool EnableLocalLogin { get; set; } //allow local login
        public bool AllowRememberConsent { get; set; } //allow remember consent on consent screen
        public bool AlwaysSendClientClaims { get; set; } //always send client claims with access token
        public bool AllowAccessToAllCustomGrantTypes { get; set; } //allow access to all grant types
        public bool AllowAccessToAllScopes { get; set; } //allow access to all scopes
        public bool AllowClientCredentialsOnly { get; set; } //allow client credentials only 
        public bool PrefixClientClaims { get; set; } //add prefix to client claims
        public bool IncludeJwtId { get; set; } //include Jwt id in Jwt access token
        public bool UpdateAccessTokenClaimsOnRefresh { get; set; } //Update token on refresh
        public List<Claim> Claims { get; set; }
        public List<Secret> ClientSecrets { get; set; }
        public List<string> IdentityProviderRestrictions { get; set; }
        public List<string> PostLogoutRedirectUris { get; set; } 
        public List<string> RedirectUris { get; set; } 
        public List<string> AllowedCorsOrigins { get; set; }
        public List<string> AllowedCustomGrantTypes { get; set; } 
        public List<string> AllowedScopes { get; set; }        
    }

}