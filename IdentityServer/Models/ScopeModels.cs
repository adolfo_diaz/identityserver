﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IdentityServer3.Core.Models;

namespace IdentityServer.Models
{
    [Table("is_Scopes")]
    public class ScopeProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool Enabled { get; set; } //Enables scope to be used
        public string Name { get; set; } //Name of the scope. This is the value a client will use to request the scope.
        public string DisplayName { get; set; } //Display name. This value will be used e.g. on the consent screen.
        public string Type { get; set; } //Specifies whether this scope is about identity information from the userinfo endpoint, or a resource (e.g. a Web API). Defaults to Resource.
        public string Claims { get; set; } //JSON String
        public string ClaimsRule { get; set; } //Rule for determining which claims should be included in the token	
        public string ScopeSecrets { get; set; } //JSON String
        public string Description { get; set; } //Description. This value will be used e.g. on the consent screen.		
        public bool IncludeAllClaimsForUser { get; set; } //If enabled, all claims for the user will be included in the token. Defaults to false.		
        public bool Emphasize { get; set; } //Specifies whether the consent screen will emphasize this scope. Use this setting for sensitive or important scopes. Defaults to false.	
        public bool ShowInDiscoveryDocument { get; set; } //Specifies whether this scope is shown in the discovery document. Defaults to true.
        public bool Required { get; set; } //Specifies whether the user can de-select the scope on the consent screen. Defaults to false.	
        public bool AllowUnrestrictedIntrospection { get; set; } //Allows access to other scopes using the introspection endpoint
    }

    public class ScopeModel
    {
        public bool IsEnabled { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; } 
        public string Type { get; set; } 
        public List<ScopeClaim> Claims { get; set; } 
        public string ClaimsRule { get; set; } 
        public List<Secret> ScopeSecrets { get; set; } 
        public string Description { get; set; }
        public bool IncludeAllClaimsForUser { get; set; } 		
        public bool Emphasize { get; set; } 	
        public bool ShowInDiscoveryDocument { get; set; } 
        public bool Required { get; set; } 	
        public bool AllowUnrestrictedIntrospection { get; set; } 
    }

}