﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using IdentityServer.Models;
using IdentityServer3.Core.Models;
using Newtonsoft.Json;
using System.Security.Claims;
using IdentityServer3.Core;

namespace IdentityServer.Helpers
{
    public static class Utils
    {
        public static bool IsNull(string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static bool IsNotNull(string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        public static string Encrypt(string clearText)
        {
            const string encryptionKey = "876B5046-D235-4475-BC29-BDB99BE601F7";
            var clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor == null) return null;
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            const string encryptionKey = "876B5046-D235-4475-BC29-BDB99BE601F7";
            var cipherBytes = Convert.FromBase64String(cipherText);
            using (var encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor == null) return null;
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static LocalUserModel LoginResponse(LocalAuthenticationContext user)
        {
            return null;
        }

        public static IEnumerable<Claim> StringToClaims(string cItems)
        {
            if (IsNull(cItems)) return null;

            try
            {
                var cList = JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(cItems);

                return cList.Any() ? BuildClaimList(cList) : null;
            }
            catch
            {
                return null;
            }

        }
        private static List<Claim> BuildClaimList(List<KeyValuePair<string, string>> cList)
        {
            if (!cList.Any()) return null;

            var claimList = cList.Select(GetClaim).Where(tmpClaim => tmpClaim != null).ToList();

            return claimList;

        }
        private static Claim GetClaim(KeyValuePair<string, string> cItem)
        {

            switch (cItem.Key.ToUpper())
            {
                case "ADDRESS":
                    return new Claim(Constants.ClaimTypes.Address, cItem.Value);
                case "BIRTHDATE":
                    return new Claim(Constants.ClaimTypes.BirthDate, cItem.Value);
                case "EMAIL":
                    return new Claim(Constants.ClaimTypes.Email, cItem.Value);
                case "EMAILVERIFIED":
                    return new Claim(Constants.ClaimTypes.EmailVerified, cItem.Value);
                case "EXPIRATION":
                    return new Claim(Constants.ClaimTypes.Expiration, cItem.Value);
                case "EXTERNALPROVIDERUSERID":
                    return new Claim(Constants.ClaimTypes.ExternalProviderUserId, cItem.Value);
                case "FAMILYNAME":
                    return new Claim(Constants.ClaimTypes.FamilyName, cItem.Value);
                case "GENDER":
                    return new Claim(Constants.ClaimTypes.Gender, cItem.Value);
                case "GIVENNAME":
                    return new Claim(Constants.ClaimTypes.GivenName, cItem.Value);
                case "MIDDLENAME":
                    return new Claim(Constants.ClaimTypes.MiddleName, cItem.Value);
                case "NAME":
                    return new Claim(Constants.ClaimTypes.Name, cItem.Value);
                case "NICKNAME":
                    return new Claim(Constants.ClaimTypes.NickName, cItem.Value);
                case "PHONENUMBER":
                    return new Claim(Constants.ClaimTypes.PhoneNumber, cItem.Value);
                case "PICTURE":
                    return new Claim(Constants.ClaimTypes.Picture, cItem.Value);
                case "ROLE":
                    return new Claim(Constants.ClaimTypes.Role, cItem.Value);
                case "SCOPE":
                    return new Claim(Constants.ClaimTypes.Scope, cItem.Value);
                case "CLIENTID":
                    return new Claim(Constants.ClaimTypes.ClientId, cItem.Value);
                default:
                    return null;
            }

        }

        public static string ClaimModelToString(ClaimModel cModel)
        {
            var claims = new List<KeyValuePair<string, string>>();

            if (cModel == null) return JsonConvert.SerializeObject(claims);

            if (IsNotNull(cModel.FirstName)) claims.Add(new KeyValuePair<string, string>("GivenName", cModel.FirstName));
            if (IsNotNull(cModel.MiddleName)) claims.Add(new KeyValuePair<string, string>("MiddleName", cModel.MiddleName));
            if (IsNotNull(cModel.LastName)) claims.Add(new KeyValuePair<string, string>("FamilyName", cModel.LastName));
            if (IsNotNull(cModel.FullName)) claims.Add(new KeyValuePair<string, string>("Name", cModel.FullName));
            if (IsNotNull(cModel.NickName)) claims.Add(new KeyValuePair<string, string>("NickName", cModel.NickName));
            if (IsNotNull(cModel.Address)) claims.Add(new KeyValuePair<string, string>("Address", cModel.Address));
            if (cModel.BirthDate.HasValue) claims.Add(new KeyValuePair<string, string>("BirthDate", cModel.BirthDate.Value.ToString("yyyy-MM-dd")));
            if (IsNotNull(cModel.Email)) claims.Add(new KeyValuePair<string, string>("Email", cModel.Email));

            claims.Add(new KeyValuePair<string, string>("EmailVerified", cModel.EmailVerified ? "True" : "False"));

            if (cModel.Expiration.HasValue)
                claims.Add(new KeyValuePair<string, string>("Expiration", cModel.Expiration.Value.ToString("yyyy-MM-dd HH:mm:ss.fff")));

            if (cModel.ExternalProviderUserIds != null)
                claims.AddRange(from userId in cModel.ExternalProviderUserIds
                    where IsNotNull(userId)
                                select new KeyValuePair<string, string>("ExternalProviderUserId", userId));

            if (IsNotNull(cModel.Gender)) claims.Add(new KeyValuePair<string, string>("Gender", cModel.Gender));
            if (IsNotNull(cModel.PhoneNumber)) claims.Add(new KeyValuePair<string, string>("PhoneNumber", cModel.PhoneNumber));
            if (IsNotNull(cModel.PhotoUrl)) claims.Add(new KeyValuePair<string, string>("Picture", cModel.PhotoUrl));

            if (cModel.Roles != null)
                claims.AddRange(from role in cModel.Roles
                    where IsNotNull(role)
                                select new KeyValuePair<string, string>("Role", role));

            if (cModel.Scopes != null)
                claims.AddRange(from scope in cModel.Scopes
                    where IsNotNull(scope)
                                select new KeyValuePair<string, string>("Scope", scope));

            if (cModel.Clients != null)
                claims.AddRange(from client in cModel.Clients
                    where IsNotNull(client)
                                select new KeyValuePair<string, string>("ClientId", client));

            var jObj = JsonConvert.SerializeObject(claims);

            return jObj;
        }

        public static ClaimModel ClaimsToClaimModel(List<Claim> cList)
        {
            var claims = new ClaimModel()
            {
                ExternalProviderUserIds = new List<string>(),
                Roles = new List<string>(),
                Clients = new List<string>(),
                Scopes = new List<string>()
            };

            foreach (var cItem in cList)
            {
                switch (cItem.Type)
                {
                    case Constants.ClaimTypes.Address:
                        claims.Address = cItem.Value;
                        break;
                    case Constants.ClaimTypes.BirthDate:
                        DateTime bDate;
                        if (DateTime.TryParse(cItem.Value, out bDate)) claims.BirthDate = bDate;
                        break;
                    case Constants.ClaimTypes.Email:
                        claims.Email = cItem.Value;
                        break;
                    case Constants.ClaimTypes.EmailVerified:
                        if (cItem.Value == "True") claims.EmailVerified = true;
                        if (cItem.Value == "False") claims.EmailVerified = false;
                        break;
                    case Constants.ClaimTypes.Expiration:
                        DateTime eDate;
                        if (DateTime.TryParse(cItem.Value, out eDate)) claims.Expiration = eDate;
                        break;
                    case Constants.ClaimTypes.ExternalProviderUserId:
                        claims.ExternalProviderUserIds.Add(cItem.Value);
                        break;
                    case Constants.ClaimTypes.FamilyName:
                        claims.LastName = cItem.Value;
                        break;
                    case Constants.ClaimTypes.Gender:
                        claims.Gender = cItem.Value;
                        break;
                    case Constants.ClaimTypes.GivenName:
                        claims.FirstName = cItem.Value;
                        break;
                    case Constants.ClaimTypes.MiddleName:
                        claims.MiddleName = cItem.Value;
                        break;
                    case Constants.ClaimTypes.Name:
                        claims.FullName = cItem.Value;
                        break;
                    case Constants.ClaimTypes.NickName:
                        claims.NickName = cItem.Value;
                        break;
                    case Constants.ClaimTypes.PhoneNumber:
                        claims.PhoneNumber = cItem.Value;
                        break;
                    case Constants.ClaimTypes.Picture:
                        claims.PhotoUrl = cItem.Value;
                        break;
                    case Constants.ClaimTypes.Role:
                        claims.Roles.Add(cItem.Value);
                        break;
                    case Constants.ClaimTypes.Scope:
                        claims.Scopes.Add(cItem.Value);
                        break;
                    case Constants.ClaimTypes.ClientId:
                        claims.Clients.Add(cItem.Value);
                        break;
                }
            }
            return claims;
        }

        public static ClaimModel StringToClaimModel(string cItems)
        {
            return ClaimsToClaimModel(StringToClaims(cItems).ToList());
        }

        public static ClaimModel GetClaimsSubset(ClaimModel currClaims, ClaimModel newClaims)
        {
            if (IsNull(currClaims.Address) && IsNotNull(newClaims.Address))
                currClaims.Address = newClaims.Address;

            if (IsNull(currClaims.FirstName) && IsNotNull(newClaims.FirstName))
                currClaims.FirstName = newClaims.FirstName;

            if (IsNull(currClaims.MiddleName) && IsNotNull(newClaims.MiddleName))
                currClaims.MiddleName = newClaims.MiddleName;

            if (IsNull(currClaims.LastName) && IsNotNull(newClaims.LastName))
                currClaims.LastName = newClaims.LastName;

            if (IsNull(currClaims.FullName) && IsNotNull(newClaims.FullName))
                currClaims.FullName = newClaims.FullName;

            if (IsNull(currClaims.NickName) && IsNotNull(newClaims.NickName))
                currClaims.NickName = newClaims.NickName;

            if (!currClaims.BirthDate.HasValue && newClaims.BirthDate.HasValue)
                currClaims.BirthDate = newClaims.BirthDate;

            if (IsNull(currClaims.Gender) && IsNotNull(newClaims.Gender))
                currClaims.Gender = newClaims.Gender;

            if (IsNull(currClaims.Email) && IsNotNull(newClaims.Email))
                currClaims.Email = newClaims.Email;

            if (IsNull(currClaims.PhoneNumber) && IsNotNull(newClaims.PhoneNumber))
                currClaims.PhoneNumber = newClaims.PhoneNumber;

            if (IsNull(currClaims.PhotoUrl) && IsNotNull(newClaims.PhotoUrl))
                currClaims.PhotoUrl = newClaims.PhotoUrl;

            if (newClaims.ExternalProviderUserIds != null)
            {
                if (currClaims.ExternalProviderUserIds == null) currClaims.ExternalProviderUserIds = new List<string>();
                foreach (
                    var userId in
                        newClaims.ExternalProviderUserIds.Where(
                            userId => !currClaims.ExternalProviderUserIds.Contains(userId)))
                {
                    currClaims.ExternalProviderUserIds.Add(userId);
                }
            }

            if (newClaims.Roles != null)
            {
                if (currClaims.Roles == null) currClaims.Roles = new List<string>();
                foreach (var role in newClaims.Roles.Where(role => !currClaims.Roles.Contains(role)))
                {
                    currClaims.Roles.Add(role);
                }
            }

            if (newClaims.Scopes != null)
            {
                if (currClaims.Scopes == null) currClaims.Scopes = new List<string>();
                foreach (var scope in newClaims.Scopes.Where(scope => !currClaims.Scopes.Contains(scope)))
                {
                    currClaims.Scopes.Add(scope);
                }
            }

            if (newClaims.Clients != null)
            {
                if (currClaims.Clients == null) currClaims.Clients = new List<string>();
                foreach (var client in newClaims.Clients.Where(client => !currClaims.Clients.Contains(client)))
                {
                    currClaims.Clients.Add(client);
                }
            }

            return currClaims;
        }

        public static bool HasRole(string roleName, string cItems)
        {
            if (IsNull(cItems)) return false;
            var tmpClaims = StringToClaims(cItems).ToList();
            var claims = ClaimsToClaimModel(tmpClaims);

            if (claims == null) return false;

            return claims.Roles != null && claims.Roles.Contains(roleName);
        }

        public static bool HasClient(string clientId, string cItems)
        {
            if (IsNull(cItems)) return false;
            var tmpClaims = StringToClaims(cItems).ToList();
            var claims = ClaimsToClaimModel(tmpClaims);

            if (claims == null) return false;

            return claims.Clients != null && claims.Clients.Contains(clientId);
        }

        public static bool HasScope(string scopeName, string cItems)
        {
            if (IsNull(cItems)) return false;
            var tmpClaims = StringToClaims(cItems).ToList();
            var claims = ClaimsToClaimModel(tmpClaims);

            if (claims == null) return false;

            return claims.Scopes != null && claims.Roles.Contains(scopeName);
        }

        public static ScopeProfile ScopeModelToScopeProfile(ScopeModel model)
        {
            try
            {
                var sProf = new ScopeProfile()
                {
                    Enabled = model.IsEnabled,
                    Name = model.Name,
                    DisplayName = model.DisplayName,
                    Type = model.Type,
                    Claims = ScopeClaimListToString(model.Claims),
                    ClaimsRule = model.ClaimsRule,
                    ScopeSecrets = SecretListToString(model.ScopeSecrets),
                    Description = model.Description,
                    IncludeAllClaimsForUser = model.IncludeAllClaimsForUser,
                    Emphasize = model.Emphasize,
                    ShowInDiscoveryDocument = model.ShowInDiscoveryDocument,
                    Required = model.Required,
                    AllowUnrestrictedIntrospection = model.AllowUnrestrictedIntrospection
                };

                return sProf;
            }
            catch
            {
                return null;
            }
        }

        public static ScopeModel ScopeProfileToScopeModel(ScopeProfile model)
        {
            try
            {
                var sItem = new ScopeModel()
                {
                    IsEnabled = model.Enabled,
                    Name = model.Name,
                    DisplayName = model.DisplayName,
                    Type = model.Type,
                    Claims = StringToScopeClaimList(model.Claims),
                    ClaimsRule = model.ClaimsRule,
                    ScopeSecrets = StringToSecretList(model.ScopeSecrets),
                    Description = model.Description,
                    IncludeAllClaimsForUser = model.IncludeAllClaimsForUser,
                    Emphasize = model.Emphasize,
                    ShowInDiscoveryDocument = model.ShowInDiscoveryDocument,
                    Required = model.Required,
                    AllowUnrestrictedIntrospection = model.AllowUnrestrictedIntrospection
                };

                return sItem;
            }
            catch
            {
                return null;
            }
        }

        public static List<ScopeClaim> StringToScopeClaimList(string value)
        {
            try
            {
                var sList = JsonConvert.DeserializeObject<List<ScopeClaim>>(value);
                return sList;
            }
            catch
            {
                return new List<ScopeClaim>();
            }
        }

        public static List<Secret> StringToSecretList(string value)
        {
            try
            {
                var sItem = JsonConvert.DeserializeObject<List<Secret>>(value);
                return sItem;
            }
            catch
            {
                return new List<Secret>();
            }
        }

        public static string ScopeClaimListToString(List<ScopeClaim> scList)
        {
            try
            {
                var sItem = JsonConvert.SerializeObject(scList);
                return sItem;
            }
            catch
            {
                return "";
            }
        }

        public static string SecretListToString(List<Secret> sList)
        {
            try
            {
                var sItem = JsonConvert.SerializeObject(sList);
                return sItem;
            }
            catch
            {
                return "";
            }
        }

        public static ClientProfile ClientModelToClientProfile(ClientModel model)
        {
            try
            {
                var cpItem = new ClientProfile()
                {
                    Enabled = model.IsEnabled,
                    ClientId = model.ClientId,
                    ClientName = model.ClientName,
                    ClientUri = model.ClientUri,
                    Flow = model.Flow,
                    LogoUri = model.LogoUri,
                    LogoutUri = model.LogoutUri,
                    RefreshTokenExpiration = model.RefreshTokenExpiration,
                    RefreshTokenUsage = model.RefreshTokenUsage,
                    AccessTokenType = model.AccessTokenType,
                    SlidingRefreshTokenLifetime = model.SlidingRefreshTokenLifetime,
                    AbsoluteRefreshTokenLifetime = model.AbsoluteRefreshTokenLifetime,
                    AccessTokenLifetime = model.AccessTokenLifetime,
                    IdentityTokenLifetime = model.IdentityTokenLifetime,
                    AuthorizationCodeLifetime = model.AuthorizationCodeLifetime,
                    RequireConsent = model.RequireConsent,
                    LogoutSessionRequired = model.LogoutSessionRequired,
                    EnableLocalLogin = model.EnableLocalLogin,
                    AllowRememberConsent = model.AllowRememberConsent,
                    AlwaysSendClientClaims = model.AlwaysSendClientClaims,
                    AllowAccessToAllCustomGrantTypes = model.AllowAccessToAllCustomGrantTypes,
                    AllowAccessToAllScopes = model.AllowAccessToAllScopes,
                    AllowClientCredentialsOnly = model.AllowClientCredentialsOnly,
                    PrefixClientClaims = model.PrefixClientClaims,
                    IncludeJwtId = model.IncludeJwtId,
                    UpdateAccessTokenClaimsOnRefresh = model.UpdateAccessTokenClaimsOnRefresh,
                    Claims = ClaimListToString(model.Claims),
                    ClientSecrets = SecretListToString(model.ClientSecrets),
                    IdentityProviderRestrictions = StringListToString(model.IdentityProviderRestrictions),
                    PostLogoutRedirectUris = StringListToString(model.PostLogoutRedirectUris),
                    RedirectUris = StringListToString(model.RedirectUris),
                    AllowedCorsOrigins = StringListToString(model.AllowedCorsOrigins),
                    AllowedCustomGrantTypes = StringListToString(model.AllowedCustomGrantTypes),
                    AllowedScopes = StringListToString(model.AllowedScopes)
                };
                return cpItem;
            }
            catch
            {
                return new ClientProfile();
            }
        }

        public static ClientModel ClientProfileToClientModel(ClientProfile model)
        {
            try
            {
                var cItem = new ClientModel()
                {
                    IsEnabled = model.Enabled,
                    ClientId = model.ClientId,
                    ClientName = model.ClientName,
                    ClientUri = model.ClientUri,
                    Flow = model.Flow,
                    LogoUri = model.LogoUri,
                    LogoutUri = model.LogoutUri,
                    RefreshTokenExpiration = model.RefreshTokenExpiration,
                    RefreshTokenUsage = model.RefreshTokenUsage,
                    AccessTokenType = model.AccessTokenType,
                    SlidingRefreshTokenLifetime = model.SlidingRefreshTokenLifetime,
                    AbsoluteRefreshTokenLifetime = model.AbsoluteRefreshTokenLifetime,
                    AccessTokenLifetime = model.AccessTokenLifetime,
                    IdentityTokenLifetime = model.IdentityTokenLifetime,
                    AuthorizationCodeLifetime = model.AuthorizationCodeLifetime,
                    RequireConsent = model.RequireConsent,
                    LogoutSessionRequired = model.LogoutSessionRequired,
                    EnableLocalLogin = model.EnableLocalLogin,
                    AllowRememberConsent = model.AllowRememberConsent,
                    AlwaysSendClientClaims = model.AlwaysSendClientClaims,
                    AllowAccessToAllCustomGrantTypes = model.AllowAccessToAllCustomGrantTypes,
                    AllowAccessToAllScopes = model.AllowAccessToAllScopes,
                    AllowClientCredentialsOnly = model.AllowClientCredentialsOnly,
                    PrefixClientClaims = model.PrefixClientClaims,
                    IncludeJwtId = model.IncludeJwtId,
                    UpdateAccessTokenClaimsOnRefresh = model.UpdateAccessTokenClaimsOnRefresh,
                    Claims = StringToClaimList(model.Claims),
                    ClientSecrets = StringToSecretList(model.ClientSecrets),
                    IdentityProviderRestrictions = StringToStringList(model.IdentityProviderRestrictions),
                    PostLogoutRedirectUris = StringToStringList(model.PostLogoutRedirectUris),
                    RedirectUris = StringToStringList(model.RedirectUris),
                    AllowedCorsOrigins = StringToStringList(model.AllowedCorsOrigins),
                    AllowedCustomGrantTypes = StringToStringList(model.AllowedCustomGrantTypes),
                    AllowedScopes = StringToStringList(model.AllowedScopes)
                };
                return cItem;
            }
            catch
            {
                return new ClientModel();
            }
        }

        public static List<Claim> StringToClaimList(string value)
        {
            try
            {
                var sItem = JsonConvert.DeserializeObject<List<Claim>>(value);
                return sItem;
            }
            catch
            {
                return new List<Claim>();
            }
        }

        public static string ClaimListToString(List<Claim> cList)
        {
            try
            {
                var sItem = JsonConvert.SerializeObject(cList);
                return sItem;
            }
            catch
            {
                return "";
            }
        }

        public static List<string> StringToStringList(string value)
        {
            try
            {
                var sItem = JsonConvert.DeserializeObject<List<string>>(value);
                return sItem;
            }
            catch
            {
                return new List<string>();
            }
        }

        public static string StringListToString(List<string> sList)
        {
            try
            {
                var sItem = JsonConvert.SerializeObject(sList);
                return sItem;
            }
            catch
            {
                return "";
            }
        }

    }
}