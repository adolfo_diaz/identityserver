define(function (require) {
    var app = require('durandal/app'),
        ko = require('knockout'),
        userEditor = require('./userEditor'),
        identityServer = require('identityServer');


    return {
        userEditor: userEditor,
        editUser: function (e) {

            var user = e.sender.dataItem(e.sender.select());

            this.userEditor.edit(user);
        },
        users: new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    identityServer.get("user/getuserlist", "json", function (result) {
                        options.success(JSON.parse(result));
                    });
                }
            },
            batch: true,
            pageSize: 20
        })
    };
});