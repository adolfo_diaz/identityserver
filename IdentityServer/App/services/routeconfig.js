﻿/** 
 * @module Route table
 */
define(function () {

    var routes = {

        //*************************************************************
        //Breeze Routes                                              **
        //*************************************************************
        lookupUrl: "durandalauth/lookups",
        saveChangesUrl: "durandalauth/savechanges",
        publicArticlesUrl: "durandalauth/publicarticles",
        privateArticlesUrl: "durandalauth/privatearticles",
        categoriesUrl: "durandalauth/categories",

        //*************************************************************
        // Account Controller                                        **
        //*************************************************************
        login: "/api/user/userlogin",
        logout: "/api/user/userlogout",
        getCurrentUserName: "/api/account/getcurrentusername",
        isAuthenticatedUser: "/api/account/isauthenticateduser",
        getSessionState: "/api/account/getsessionstate"

    };

    return routes;

});