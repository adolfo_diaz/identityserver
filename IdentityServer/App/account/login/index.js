﻿define(['plugins/router', 'jquery', 'durandal/app', 'services/routeconfig', 'knockout','services/appsecurity'], function (router, $, app, routeconfig, ko,appSecurity) {

    var userName = ko.observable();
    var userIdentity = ko.observable();
    var errorMessage = ko.observable();

    function setErrorMessage(error) {
        $("#login-error-message").css("display", "block").html(error);
        setTimeout(function () {
            $("#login-error-message").css("display", "none");
        }, 5000);
    }

    function login() {

        $("#is-login-wait").show();

        var uName = $("#is-login-username").val();

        var obj = {
            UserName: uName,
            Password: $("#is-login-password").val(),
            ClientId: "idenserv"
        };

        $.ajax(routeconfig.login, {
            type: "POST",
            data: obj,
            success: function (jqXhr, error, statusText) {

                switch (jqXhr) {
                    case "Missing User Name":
                        setErrorMessage(statusText.responseText);
                        break;
                    case "Missing Password":
                        setErrorMessage(statusText.responseText);
                        break;
                    case "Client And/Or Scope Is Required":
                        setErrorMessage(statusText.responseText);
                        break;
                    case "Username/Password Incorrect":
                        setErrorMessage(statusText.responseText);
                        break;
                    case "Not Authorized":
                        setErrorMessage(statusText.responseText);
                        break;
                    default:
                        if (jqXhr) {
                            userIdentity(JSON.parse(jqXhr));
                        }
                        appSecurity.setAuth('Authenticated', 'true', 1);
                        router.reset();
                        router.deactivate();
                        app.setRoot('shell', 'entrance');
                        $("#loginbutton").hide();
                        $("#logoutbutton").show();
                        break;
                }
            },
            error: function (jqXhr, error, statusText) {
                $("#is-login-wait").hide();
            }
        });

    };

    var viewmodel = {
        canActivate: function () {
            return true;
        },
        activate: function () {

        },
        compositionComplete: function () {
        },
        login: login,
        errorMessage: errorMessage
    };

    return viewmodel;
});