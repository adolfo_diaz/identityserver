﻿using System;
using System.Collections.Generic;
using System.Linq;
using IdentityServer.Models;
using Newtonsoft.Json;
using NUnit.Framework;

namespace IdentityServer.Test.Controllers
{
    [TestFixture]
    public class ScopeController : TestBase
    {
        [Test]
        public async void Get_Scope_Details()
        {
            var response = await Client.GetScopeDetails("testApi");
            var jObj = JsonConvert.DeserializeObject(response);
            var uObj = JsonConvert.DeserializeObject<ScopeModel>(jObj.ToString());
            Assert.IsNotNull(uObj);
            Assert.AreEqual("testApi", uObj.Name);

            Console.WriteLine(@"Scope Name: " + uObj.Name);
        }

        [Test]
        public async void Get_Scope_List()
        {
            var response = await Client.GetScopeList();
            var jObj = JsonConvert.DeserializeObject(response);
            var uObj = JsonConvert.DeserializeObject<List<ScopeModel>>(jObj.ToString());
            Assert.IsNotNull(uObj);

            Console.WriteLine(@"Scope Count: " + uObj.Count());

            if (!uObj.Any()) return;

            var uItem = uObj.FirstOrDefault(a => a.Name == "testApi");
            Assert.IsNotNull(uItem);

            Console.WriteLine(@"First Scope: " + uObj[0].Name);
        }

        [Test]
        public async void Add_And_Delete_New_Scope()
        {
            //New scope model
            var nScope = new ScopeModel()
            {
                Name = "test_scope",
                DisplayName = "Test Scope",
                Type = "Resource"
            };

            //Add new scope
            var response = await Client.AddScope(nScope);
            Assert.IsTrue(response);

            //Get new scope details
            var response1 = await Client.GetScopeDetails(nScope.Name);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<ScopeModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nScope.Name, uObj1.Name);
            Console.WriteLine(@"New Scope: " + uObj1.Name);

            //Delete new scope
            var response2 = await Client.DeleteScope(nScope);
            Assert.IsTrue(response2);

            //Verify deleted new scope
            var response3 = await Client.GetScopeList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<ScopeModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.Name == nScope.Name);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted Scope: " + nScope.Name);
        }

        [Test]
        public async void Disable_And_Enable_New_Scope()
        {
            //New scope model
            var nScope = new ScopeModel()
            {
                Name = "test_scope1",
                DisplayName = "Test Scope1",
                Type = "Resource"
            };

            //Add new scope
            var response = await Client.AddScope(nScope);
            Assert.IsTrue(response);

            //Get new scope details
            var response1 = await Client.GetScopeDetails(nScope.Name);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<ScopeModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nScope.Name, uObj1.Name);
            Console.WriteLine(@"New Scope: " + uObj1.Name);

            //Disable new scope
            var response4 = await Client.DisableScope(nScope);
            Assert.IsTrue(response4);

            //Verify new scope is disabled
            var response5 = await Client.GetScopeDetails(nScope.Name);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<ScopeModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            Assert.AreEqual(nScope.Name, uObj5.Name);
            Assert.AreEqual(false, uObj5.IsEnabled);
            Console.WriteLine(@"Disabled Scope: " + uObj5.Name);

            //Enable new scope
            var response6 = await Client.EnableScope(nScope);
            Assert.IsTrue(response6);

            //Verify new scope is enabled
            var response7 = await Client.GetScopeDetails(nScope.Name);
            var jObj7 = JsonConvert.DeserializeObject(response7);
            var uObj7 = JsonConvert.DeserializeObject<ScopeModel>(jObj7.ToString());
            Assert.IsNotNull(uObj7);
            Assert.AreEqual(nScope.Name, uObj7.Name);
            Assert.AreEqual(true, uObj7.IsEnabled);
            Console.WriteLine(@"Enabled Scope: " + uObj7.Name);

            //Delete new scope
            var response2 = await Client.DeleteScope(nScope);
            Assert.IsTrue(response2);

            //Verify deleted new scope
            var response3 = await Client.GetScopeList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<ScopeModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.Name == nScope.Name);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted Scope: " + nScope.Name);
        }

        [Test]
        public async void Update_New_Scope()
        {
            //New scope model
            var nScope = new ScopeModel()
            {
                Name = "test_scope2",
                DisplayName = "Test Scope2",
                Type = "Resource"
            };

            //Add new scope
            var response = await Client.AddScope(nScope);
            Assert.IsTrue(response);

            //Get new scope details
            var response1 = await Client.GetScopeDetails(nScope.Name);
            var jObj1 = JsonConvert.DeserializeObject(response1);
            var uObj1 = JsonConvert.DeserializeObject<ScopeModel>(jObj1.ToString());
            Assert.IsNotNull(uObj1);
            Assert.AreEqual(nScope.Name, uObj1.Name);
            Console.WriteLine(@"New Scope: " + uObj1.Name);

            //Update new scope
            var tmpScope = nScope;
            tmpScope.DisplayName = "Updated Scope";
            tmpScope.Type = "Identity";
            var response4 = await Client.UpdateScope(tmpScope);
            Assert.IsTrue(response4);

            //Verify update to new scope details
            var response5 = await Client.GetScopeDetails(nScope.Name);
            var jObj5 = JsonConvert.DeserializeObject(response5);
            var uObj5 = JsonConvert.DeserializeObject<ScopeModel>(jObj5.ToString());
            Assert.IsNotNull(uObj5);
            Assert.AreEqual(tmpScope.Name, uObj5.Name);
            Assert.AreEqual(tmpScope.DisplayName, uObj5.DisplayName);
            Assert.AreEqual(tmpScope.Type, uObj5.Type);
            Console.WriteLine(@"Updated Scope: " + uObj5.Name);

            //Delete new scope
            var response2 = await Client.DeleteScope(nScope);
            Assert.IsTrue(response2);

            //Verify deleted new scope
            var response3 = await Client.GetScopeList();
            var jObj3 = JsonConvert.DeserializeObject(response3);
            var uObj3 = JsonConvert.DeserializeObject<List<ScopeModel>>(jObj3.ToString());
            Assert.IsNotNull(uObj3);
            if (!uObj3.Any()) return;
            var uItem = uObj3.FirstOrDefault(a => a.Name == nScope.Name);
            Assert.IsNull(uItem);
            Console.WriteLine(@"Deleted Scope: " + nScope.Name);
        }
    }
}
