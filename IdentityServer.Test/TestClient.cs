﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using IdentityServer.Helpers;
using Newtonsoft.Json;
using IdentityServer.Models;
using NUnit.Framework;

namespace IdentityServer.Test
{
    public class TestClient
    {
        private readonly ClientSetupModel _model;
        private HttpClient _client;
        private LocalUserModel _userModel;

        public TestClient(ClientSetupModel model)
        {
            _model = model;

        }

        private HttpClient GetHttpClient()
        {
            return _client ?? (_client = new HttpClient() { BaseAddress = new Uri(_model.ServerUrl) });
        }

        public async Task<bool> LoginLocalUser(UserLoginModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/userlogin",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                var content = await response.Content.ReadAsStringAsync();

                var jObj1 = JsonConvert.DeserializeObject(content);
                var uObj1 = JsonConvert.DeserializeObject<LocalUserModel>(jObj1.ToString());
                Assert.IsNotNull(uObj1);

                _userModel = uObj1;

                Console.WriteLine(@"User Login: " + response.StatusCode);
                Console.WriteLine(@"Username: " + uObj1.UserName);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public async Task<bool> LogoutLocalUser()
        {
            try
            {
                _client = GetHttpClient();

                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(_userModel);

                var response = await _client.PostAsync("api/user/userlogout",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"User Logout: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> LoginClientUser(ClientLoginModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/clientlogin",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                var content = await response.Content.ReadAsStringAsync();

                _model.ClientToken = content;

                Console.WriteLine(@"Client Login: " + response.StatusCode);
                Console.WriteLine(@"Token: " + content);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<string> GetUserDetails(string userName)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var response = await _client.GetAsync("api/user/getuserdetails?userName=" + userName);

                var content = await response.Content.ReadAsStringAsync();

                return content;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<string> GetUserList(string userRoleName = "", string userClientId = "", string userScope = "")
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var pItems = "";

                if (Utils.IsNotNull(userRoleName)) pItems += "?userRoleName=" + userRoleName;
                if (Utils.IsNull(pItems) && Utils.IsNotNull(userClientId)) pItems += "?userClientId=" + userClientId;
                if (Utils.IsNotNull(pItems) && Utils.IsNotNull(userClientId)) pItems += "&userClientId=" + userClientId;

                if (Utils.IsNull(pItems) && Utils.IsNotNull(userScope)) pItems += "?userScope=" + userScope;
                if (Utils.IsNotNull(pItems) && Utils.IsNotNull(userScope)) pItems += "&userScope=" + userScope;

                var response = await _client.GetAsync("api/user/getuserlist" + pItems);

                var content = await response.Content.ReadAsStringAsync();

                return content;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<bool> AddUser(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/adduser",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Add User: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateUser(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/updateuser",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Update User: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EnableUser(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/enableuser",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Enable User: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DisableUser(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/disableuser",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Disable User: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> SetUserExpiration(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/setuserexpiration",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Set User Expiration: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DeleteUser(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/deleteuser",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Delete User: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> AddUserClients(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/adduserclients",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Add User Clients: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> RemoveUserClients(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/removeuserclients",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Remove User Clients: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> AddUserRoles(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/adduserroles",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Add User Roles: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> RemoveUserRoles(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/removeuserroles",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Remove User Roles: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> AddUserScopes(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/adduserscopes",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Add User Scopes: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> RemoveUserScopes(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/removeuserscopes",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Remove User Scopes: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> AddUserExternalUserIds(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/adduserexternaluserids",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Add User External User Ids: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> RemoveUserExternalUserIds(LocalUserModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/removeuserexternaluserids",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Remove User External User Ids: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> ChangeUserPassword(UserChangePasswordModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/user/changeuserpassword",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Change User Password: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<string> GetScopeDetails(string scopeName)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var response = await _client.GetAsync("api/scope/getscopedetails?scopeName=" + scopeName);

                var content = await response.Content.ReadAsStringAsync();

                return content;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<string> GetScopeList()
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var response = await _client.GetAsync("api/scope/getscopelist");

                var content = await response.Content.ReadAsStringAsync();

                return content;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<bool> AddScope(ScopeModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/scope/addscope",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Add Scope: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateScope(ScopeModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/scope/updatescope",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Update Scope: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EnableScope(ScopeModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/scope/enablescope",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Enable Scope: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DisableScope(ScopeModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/scope/disablescope",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Disable Scope: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DeleteScope(ScopeModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/scope/deletescope",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Delete Scope: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<string> GetClientDetails(string clientId)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var response = await _client.GetAsync("api/client/getclientdetails?clientId=" + clientId);

                var content = await response.Content.ReadAsStringAsync();

                return content;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<string> GetClientList()
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var response = await _client.GetAsync("api/client/getclientlist");

                var content = await response.Content.ReadAsStringAsync();

                return content;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public async Task<bool> AddClient(ClientModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/client/addclient",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Add Client: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateClient(ClientModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/client/updateclient",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Update Client: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EnableClient(ClientModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/client/enableclient",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Enable Client: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DisableClient(ClientModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/client/disableclient",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Disable Client: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DeleteClient(ClientModel model)
        {
            try
            {
                _client = GetHttpClient();
                _client.DefaultRequestHeaders.Accept.Clear();

                var jObj = JsonConvert.SerializeObject(model);

                var response = await _client.PostAsync("api/client/deleteclient",
                    new StringContent(jObj, Encoding.UTF8, "application/json"));

                Console.WriteLine(@"Delete Client: " + response.StatusCode);

                return response.StatusCode.ToString() == "OK";
            }
            catch
            {
                return false;
            }
        }

    }
}
